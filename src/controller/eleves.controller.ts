import {Request, Response} from "express";
import ElevesService from "../service/eleves.service";
import {log} from "util";

export default class ElevesController {

    private service: ElevesService;

    constructor(service: ElevesService) {
        this.service = service
        console.log(this.service)
    }

    public getAll = async (req :Request, res: Response) => {
        const data = await this.service.getAll().then((data: any) => data);
        console.log(data.id)
        res.send(data);
    }

    public addEleve = async  (req :Request, res: Response) => {
        const eleve = req.body
        const data = await this.service.saveEleve(eleve).then((data:any) => data)
        res.send(data)
    }

    public getById = async (req: Request, res: Response) => {
        const id = req.params.id
        const data = await this.service.getByid(id).then(data => data).catch(err => err);
        res.send(data)
    }

        public getByClasse = async (req: Request, res: Response) => {
            const idClasse = req.params.idClasse
            const data = await this.service.getbyClasse(idClasse).then(data => data);
            res.send(data)
        }

        public deleteById = async (req: Request, res: Response) => {
            const id = req.params.id
            this.service.deleteById(id);
            res.send("element delete");

        }

        public update = async (req: Request, res: Response) => {
            const id = req.params.id
            const eleve = req.body
            const data = await this.service.update(id, eleve).then(data => data).catch(err => err)
            console.log(data)
            res.send(data)
        }

}