import express from "express";
import ElevesService from "../service/eleves.service";
import eleveModel from "../repository/eleves.repository";
import ElevesController from "../controller/eleves.controller";

const service = new ElevesService(eleveModel);
const controller = new ElevesController(service)

const router = express.Router()

router.get("/", controller.getAll)
router.get("/:id", controller.getById)
router.get("/classe/:idClasse", controller.getByClasse)
router.post("/", controller.addEleve)
router.delete("/:id", controller.deleteById)
router.put("/:id", controller.update)

export default router;