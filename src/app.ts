import express, {Request, Response} from 'express'
import "dotenv/config"
import mongoose from "mongoose";
import router from "./router/eleves.router";

mongoose.connect(`${process.env.mongoURI}`);
mongoose.set('strictQuery', false);

const app = express();
app.use(express.json())
app.use(express.urlencoded())
app.use("/eleves",router);
const port = process.env.PORT


app.listen(port, () => {
    console.log(`je suis connecté sur le port ${port}`)
})