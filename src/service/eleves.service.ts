import eleveModel from "../repository/eleves.repository";
import {elevesType} from "../DAO/eleves.dao";


export default class ElevesService {

    private repo: typeof eleveModel;

    constructor(repository: typeof eleveModel) {
        this.repo = repository
    }

    public async getAll(): Promise<elevesType[]> {
        return this.repo.find();
    }

    public async saveEleve(eleve: elevesType): Promise<elevesType> {
        return this.repo.create(eleve);
    }

    public async getByid(id: String): Promise<elevesType> {
        return this.repo.findById(id).catch(err => err);
    }

    public async getbyClasse(classe: String): Promise<elevesType[]> {
        return await this.repo.find({classe: classe})
    }

    public deleteById(id: String): void {
        this.repo.deleteOne({_id: id}).then(data => data);
    }

    public async update(id: String, eleve: elevesType): Promise<elevesType> {
        this.repo.findByIdAndUpdate({_id: id}, eleve).then(data => data).catch(err => err);
        return this.getByid(id);
    }

}