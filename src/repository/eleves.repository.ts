import * as mongoose from "mongoose";
import {eleveSchema} from "../DAO/eleves.dao";

const eleveModel = mongoose.model("eleves", eleveSchema, "eleves");
export default eleveModel;