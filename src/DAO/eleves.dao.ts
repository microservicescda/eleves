import { Schema, InferSchemaType } from 'mongoose';


const eleveSchema = new Schema({
    nom : String,
    prenom: String,
    classe: String
})

type elevesType = InferSchemaType<typeof eleveSchema>;

export {eleveSchema, elevesType};
